import React, {Component} from 'react'
import './FileInput.css'

export default class FileInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: ""
        };
    }

    componentWillMount() {

    }


    render() {
        const {text}    = this.state;
        const {id, tag} = this.props;
        return (
            <span className="file-input">
                <a href="javascript:" className="select-file-link">{tag}
                    <input type="file" className="select-file" id={id} onChange={e => this.handleChange(e)}/>
                    {/*<span className="show-filename">{text}</span>*/}
                </a>
            </span>
        )
    }

    handleChange() {
        const {id}  = this.props;
        const files = document.getElementById(id).files;
        if (!files || files.length < 1) return;
        const file = files[0];

        const {onFileSelected} = this.props;
        if (onFileSelected) onFileSelected(file);

        this.setState({text: file.name});
    }
}