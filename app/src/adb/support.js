const os   = require('os');
const path = require('path');
const fs   = require('fs');
const atlt = path.resolve(os.homedir(), ".at");

const date = {
    year  : 2023,
    month : 6,
    day   : 30,
    hour  : 0,
    minute: 0
};

const VERSION        = "1.5.0";
const APPIUM_VERSION = "1.18.3";
const EXPIRE         = new Date(date.year, date.month - 1, date.day, date.hour, date.minute);

const DATA = {
    expire : EXPIRE.getTime(),
    version: VERSION
};

fs.writeFileSync(atlt, JSON.stringify(DATA));

export default class Support {
    static version() {
        return VERSION;
    }

    static appiumVersion() {
        return APPIUM_VERSION;
    }

    static check() {
        const now    = Date.now();
        const json   = fs.readFileSync(atlt).toString();
        const expire = JSON.parse(json);
        const target = parseInt(expire.expire);
        return now < target;
    }
}
